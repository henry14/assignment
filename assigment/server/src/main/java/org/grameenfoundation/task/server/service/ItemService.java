package org.grameenfoundation.task.server.service;

import org.grameenfoundation.task.model.Item;

public interface ItemService extends BaseService<Item> {

}
