package org.grameenfoundation.task.webui.controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.grameenfoundation.task.model.Item;
import org.grameenfoundation.task.model.Market;
import org.grameenfoundation.task.model.Vendor;
import org.grameenfoundation.task.model.search.DummyItem;
import org.grameenfoundation.task.model.search.VendorSearchParams;
import org.grameenfoundation.task.server.service.ItemService;
import org.grameenfoundation.task.server.service.MarketService;
import org.grameenfoundation.task.server.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="api/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	@Autowired
	private VendorService vendorService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	// @RequestBody
	public @ResponseBody Item addItem(@RequestBody DummyItem dItem) {
		
		VendorSearchParams params = new VendorSearchParams();
		params.setFirstName(dItem.getFirstName());
		params.setLastName(dItem.getLastName());
		
		Vendor vendor = vendorService.getByName(params);
		
		Item item = new Item();
		item.setVendor(vendor);
		item.setCommodity(dItem.getCommodity());
		item.setMeasure(dItem.getMeasure());
		item.setCost(dItem.getCost());
		try {
				item.setId(null);
				itemService.save(item);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return item;

	}
	
	@RequestMapping("/get")
	public @ResponseBody List<Item> getItems(){
		List<Item> items = itemService.getAll();
		return items;
		
	}

}
