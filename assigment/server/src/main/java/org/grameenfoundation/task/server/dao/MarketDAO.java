package org.grameenfoundation.task.server.dao;

import org.grameenfoundation.task.model.Market;

public interface MarketDAO extends BaseDAO<Market> {

}
