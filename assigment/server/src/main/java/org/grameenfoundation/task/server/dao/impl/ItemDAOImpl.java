package org.grameenfoundation.task.server.dao.impl;

import org.grameenfoundation.task.model.Item;
import org.grameenfoundation.task.server.dao.ItemDAO;
import org.springframework.stereotype.Repository;

@Repository("itemDAO")
public class ItemDAOImpl extends BaseDAOImpl<Item> implements ItemDAO{

}
