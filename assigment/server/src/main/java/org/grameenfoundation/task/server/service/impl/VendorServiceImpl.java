package org.grameenfoundation.task.server.service.impl;

import java.util.List;

import org.grameenfoundation.task.model.Vendor;
import org.grameenfoundation.task.model.search.VendorSearchParams;
import org.grameenfoundation.task.server.dao.VendorDAO;
import org.grameenfoundation.task.server.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

@Transactional
@Service("vendorService")
public class VendorServiceImpl implements VendorService {
	
	@Autowired
	private VendorDAO vendorDAO;

	@Override
	public void save(Vendor t) {
		vendorDAO.save(t);
		
	}

	@Override
	public Vendor getById(String id) {
		// TODO Auto-generated method stub
		return vendorDAO.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public List<Vendor> getAll() {
		// TODO Auto-generated method stub
		return vendorDAO.findAll();
	}

	@Override
	public void deleteByIds(String[] ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Vendor getByName(VendorSearchParams params) {
		Search search = new Search();
		search.addFilterEqual("firstName", params.getFirstName());
		search.addFilterEqual("lastName", params.getLastName());
		Vendor vendor = vendorDAO.searchUnique(search);
		return vendor;
	}

}
