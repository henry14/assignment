package org.grameenfoundation.task.model.search;

public class DummyItem {
	
	private String firstName;
	private String lastName;
	private String commodity;
	private String measure;
	private Integer cost;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCommodity() {
		return commodity;
	}
	
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	
	

}
