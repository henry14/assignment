package org.grameenfoundation.task.server.dao;

import org.grameenfoundation.task.model.Item;

public interface ItemDAO extends BaseDAO<Item> {
}
