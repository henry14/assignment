package org.grameenfoundation.task.server.dao.impl;

import org.grameenfoundation.task.model.Vendor;
import org.grameenfoundation.task.server.dao.VendorDAO;
import org.springframework.stereotype.Repository;

@Repository("vendorDAO")
public class VendorDAOImpl extends BaseDAOImpl<Vendor> implements VendorDAO{

}
