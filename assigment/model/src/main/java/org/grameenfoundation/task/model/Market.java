package org.grameenfoundation.task.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="markets")
public class Market extends BaseData {

	/**
	 * @henry14
	 */
	private static final long serialVersionUID = 2960742925357806145L;
	
	private String name;

	@Column(name="name", nullable=false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
