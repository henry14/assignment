package org.grameenfoundation.task.webui.controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.grameenfoundation.task.model.Market;
import org.grameenfoundation.task.model.Vendor;
import org.grameenfoundation.task.model.search.DummyVendor;
import org.grameenfoundation.task.server.service.MarketService;
import org.grameenfoundation.task.server.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="api/vendor")
public class VendorController {
	
	@Autowired
	private VendorService vendorService;
	@Autowired
	private MarketService marketService;
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	// @RequestBody
	public @ResponseBody Vendor addVendor(@RequestBody DummyVendor dVendor) {
	
		Market market = marketService.getByName(dVendor.getMarket());
	
		Vendor vendor = new Vendor();
		vendor.setMarket(market);
		vendor.setFirstName(dVendor.getFirstName());
		vendor.setLastName(dVendor.getLastName());
		try {
			/*if (StringUtils.isNotEmpty(vendor.getId())) {
				System.out.println(vendor.getFirstName().toString());
				existing = vendorService.getById(vendor.getId());
			} else {*/
			vendor.setId(null);
				vendorService.save(vendor);
//			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return vendor;

	}
	
	@RequestMapping("get")
	public @ResponseBody List<Vendor> getVendors(){
		List<Vendor> vendors = vendorService.getAll();
		return vendors;
		
	}

}
