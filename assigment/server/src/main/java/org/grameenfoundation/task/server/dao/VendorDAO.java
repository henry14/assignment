package org.grameenfoundation.task.server.dao;

import org.grameenfoundation.task.model.Vendor;

public interface VendorDAO extends BaseDAO<Vendor> {

}
