package org.grameenfoundation.task.server.service;

import org.grameenfoundation.task.model.Market;

public interface MarketService extends BaseService<Market> {
	
	Market getByName(String market);

}
