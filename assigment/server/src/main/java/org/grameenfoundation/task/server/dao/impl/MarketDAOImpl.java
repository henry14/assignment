package org.grameenfoundation.task.server.dao.impl;

import org.grameenfoundation.task.model.Market;
import org.grameenfoundation.task.server.dao.MarketDAO;
import org.springframework.stereotype.Repository;

@Repository("marketDAO")
public class MarketDAOImpl extends BaseDAOImpl<Market> implements MarketDAO {

}
