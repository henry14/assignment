package org.grameenfoundation.task.server.service;

import org.grameenfoundation.task.model.Vendor;
import org.grameenfoundation.task.model.search.VendorSearchParams;

public interface VendorService extends BaseService<Vendor> {
	
	Vendor getByName(VendorSearchParams params);

}
