package org.grameenfoundation.task.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vendors")
public class Vendor extends BaseData {

	/**
	 * @henry14
	 */
	private static final long serialVersionUID = 7465925217947087596L;
	
	private Market market;
	private String firstName;
	private String lastName;
	
	@ManyToOne
	@JoinColumn(name="market_id", nullable=false)
	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
	}
	
	@Column(name="fname", nullable=false)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="lname", nullable=false)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

}
