package org.grameenfoundation.task.server.service.impl;

import java.util.List;

import org.grameenfoundation.task.model.Item;
import org.grameenfoundation.task.server.dao.ItemDAO;
import org.grameenfoundation.task.server.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("itemService")
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemDAO itemDAO;

	@Override
	public void save(Item t) {
		itemDAO.save(t);
		
	}

	@Override
	public Item getById(String id) {
		// TODO Auto-generated method stub
		return itemDAO.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public List<Item> getAll() {
		// TODO Auto-generated method stub
		return itemDAO.findAll();
	}

	@Override
	public void deleteByIds(String[] ids) {
		// TODO Auto-generated method stub
		
	}

}
