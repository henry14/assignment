package org.grameenfoundation.task.server.service.impl;

import java.util.List;

import org.grameenfoundation.task.model.Market;
import org.grameenfoundation.task.server.dao.MarketDAO;
import org.grameenfoundation.task.server.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("marketService")
public class MarketServiceImpl implements MarketService {

	@Autowired
	private MarketDAO marketDAO;
	
	@Override
	public void save(Market t) {
		marketDAO.save(t);
		
	}

	@Override
	public Market getById(String id) {
		// TODO Auto-generated method stub
		return marketDAO.searchUniqueByPropertyEqual("id", id);
	}

	@Override
	public List<Market> getAll() {
		// TODO Auto-generated method stub
		return marketDAO.findAll();
	}

	@Override
	public void deleteByIds(String[] ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Market getByName(String market) {
		// TODO Auto-generated method stub
		return marketDAO.searchUniqueByPropertyEqual("name", market);
	}

}
