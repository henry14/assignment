package org.grameenfoundation.task.webui.controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.grameenfoundation.task.model.Market;
import org.grameenfoundation.task.server.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="api/market")
public class MarketController {
	
	@Autowired
	private MarketService marketService;
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	// @RequestBody
	public @ResponseBody Market addMarket(@RequestBody Market market) {

		Market existing = market;
		try {
			if (StringUtils.isNotEmpty(market.getId())) {
				System.out.println(market.getName().toString());
				existing = marketService.getById(market.getId());
			} else {
				existing.setId(null);
				marketService.save(existing);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return market;

	}
	
	@RequestMapping("get")
	public @ResponseBody List<Market> getMarkets(){
		List<Market> markets = marketService.getAll();
		return markets;
		
	}

}
