package org.grameenfoundation.task.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="items")
public class Item extends BaseData {

	/**
	 * @henry14
	 */
	private static final long serialVersionUID = -1490576051472859488L;
	
	private Vendor vendor;
	private String commodity;
	private String measure;
	private Integer cost;
	
	@ManyToOne
	@JoinColumn(name="vendor_id", nullable=false)
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	
	@Column(name="item", nullable=false)
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	
	@Column(name="measure", nullable=false)
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	
	@Column(name="cost", nullable=false)
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	
	

}
